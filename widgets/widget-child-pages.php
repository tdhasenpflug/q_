<?php

class q_Child_Pages extends WP_Widget {
    private static $widget_defaults = array(
        'title' => 'Child Pages',
        'template' => '<li>[q_link]</li>',
    );
    function __construct()
    {
        parent::__construct(
            'q_child_pages',
            __('q_ Child Pages'),
            array(
                'description' => 'Display children of the current page using q_',
            )
        );
    }
    function widget($args, $instance)
    {
        $props = wp_parse_args($instance, q_Child_pages::$widget_defaults);
        
        global $post;
        $query = do_qquery(array(
            'type' => 'page',
            'status' => 'publish',
            'count' => -1,
            'parent' => $post->ID,
            'orderby' => 'menu_order',
            'order' => 'ASC'
        ), $props['template']);
        
        if (!empty($query)) {
            echo $args['before_widget'];
            echo '<h3>' . $props['title'] . '</h3>';
            echo '<ul class="q_child_pages">';
            echo $query;
            echo '</ul>';
            echo $args['after_widget'];
        }
    }
    function update($ni, $oi)
    {
        $ni['title'] = !empty($ni['title']) ? sanitize_text_field($ni['title']) : null;
        $ni['template'] = !empty($ni['template']) ? wp_kses_post($ni['template']) : null;
        
        $ni = array_filter($ni);
        
        return wp_parse_args($ni, q_Child_pages::$widget_defaults);
    }
    function form($instance)
    {
        $props = wp_parse_args($instance, q_Child_pages::$widget_defaults);
        
        echo "<p><label for='" . esc_attr($this->get_field_id('title')) . "'>Title:</label>";
        echo "<input type='text' value='" . esc_attr($props['title']) . "' class='widefat' id='" . esc_attr($this->get_field_id('title')) . "' name='" . esc_attr($this->get_field_name('title')) . "'></p>";
        
        echo "<p><label for='" . esc_attr($this->get_field_id('template')) . "'>Template:</label>";
        echo "<textarea rows=6 class='widefat' id='" . esc_attr($this->get_field_id('template')) . "' name='" . esc_attr($this->get_field_name('template')) . "'>" . esc_attr($props['template']) . "</textarea>";
        
        echo '<strong>Usable shortcodes:</strong> [q_title] [q_link] [q_url] [q_date] [q_content] [q_excerpt] [q_author] [q_terms] [q_status] [q_type] [q_id] [q_image]</p>';
    }
}
