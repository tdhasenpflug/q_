<?php

class q_Recent_Posts extends WP_Widget {
    private static $widget_defaults = array(
        'title' => 'Recent Posts',
        'template' => '<li>[q_link]</li>',
        'count' => 3
    );
    function __construct()
    {
        parent::__construct(
            'q_recent_posts',
            __('q_ Recent Posts'),
            array(
                'description' => 'Display recent posts using q_',
            )
        );
    }
    function widget($args, $instance)
    {
        $props = wp_parse_args($instance, q_Recent_Posts::$widget_defaults);
        echo $args['before_widget'];
        
        echo '<h3>' . $props['title'] . '</h3>';
        echo '<ul class="q_recent_posts">';
        echo do_qquery(array(
            'type' => 'post',
            'status' => 'publish',
            'count' => intval($props['count']),
            'orderby' => 'date',
            'order' => 'DESC'
        ), $props['template']);
        echo '</ul>';
        
        echo $args['after_widget'];
    }
    function update($ni, $oi)
    {
        $ni['title'] = !empty($ni['title']) ? sanitize_text_field($ni['title']) : null;
        $ni['count'] = !empty($ni['count']) ? intval($ni['count']) : null;
        $ni['template'] = !empty($ni['template']) ? wp_kses_post($ni['template']) : null;
        
        $ni = array_filter($ni);
        
        return wp_parse_args($ni, q_Recent_Posts::$widget_defaults);
    }
    function form($instance)
    {
        $props = wp_parse_args($instance, q_Recent_Posts::$widget_defaults);
        
        echo "<p><label for='" . esc_attr($this->get_field_id('title')) . "'>Title:</label>";
        echo "<input type='text' value='" . esc_attr($props['title']) . "' class='widefat' id='" . esc_attr($this->get_field_id('title')) . "' name='" . esc_attr($this->get_field_name('title')) . "'></p>";
        
        echo "<p><label for='" . esc_attr($this->get_field_id('count')) . "'>Count:</label>";
        echo "<input type='number' step='1' min='1' value='" . esc_attr($props['count']) . "' class='widefat' id='" . esc_attr($this->get_field_id('count')) . "' name='" . esc_attr($this->get_field_name('count')) . "'></p>";
        
        echo "<p><label for='" . esc_attr($this->get_field_id('template')) . "'>Template:</label>";
        echo "<textarea rows=6 class='widefat' id='" . esc_attr($this->get_field_id('template')) . "' name='" . esc_attr($this->get_field_name('template')) . "'>" . esc_attr($props['template']) . "</textarea>";
        
        echo '<strong>Usable shortcodes:</strong> [q_title] [q_link] [q_url] [q_date] [q_content] [q_excerpt] [q_author] [q_terms] [q_status] [q_type] [q_id] [q_image]</p>';
    }
}
