<?php

function q__widgets() {

    include 'widget-recent-posts.php';
    include 'widget-child-pages.php';

    register_widget('q_Recent_Posts');
    register_widget('q_Child_Pages');
    
    do_action('register_q_widgets');
}
add_action('widgets_init', 'q__widgets');
