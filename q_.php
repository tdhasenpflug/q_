<?php
/*
 * Plugin Name: q_
 * Plugin URI: http://taylorhasenpflug.com/q_/
 * Description: The thing that queries.
 * Author: Taylor Hasenpflug
 * Version: 0.1.4
 */

class q__ {
    private static $q_defaults = array(
        /*
         * Author parameters
         */
        'author' => null, // string
        'author_id' => null, // 1+ integers
        
        /*
         * Category Parameters
         */
        'category' => null, // string, csv/psv strings
        'category_id' => null, // 1+ integers
        
        /*
         * Tag Parameters
         */
        'tag' => null, // string, csv/psv strings
        'tag_id' => null, // int
        
        /*
         * Taxonomy queries not implemented yet
         */
        
        /*
         * Search
         */
        'search' => null, // string
        
        /*
         * Post & Page Parameters
         */
        'id' => null, // 1 integer
        'name' => null, // string (slug)
        'parent' => null, // 1 integer        
        'type' => 'any', // string
        'count' => '-1', // 1 integer
        'status' => 'any', // ('publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash', 'any')        
        
        'order' => 'ASC',
        'orderby' => 'none',
        
        'ignore_sticky' => false, // bool
    );
    const layout = "<p id='q_p-[q_id]' class='q_query'>[q_image class='alignleft'][q_link]<br /><em>By [q_author] on [q_date]</em><br />[q_excerpt]<br />Categories: [q_term]<br />Tags: [q_term term='post_tag']</p>";
    
    public function __construct()
    {
        add_shortcode('q_', array($this, 'shortcode'));
    }
    public function shortcode($atts, $content = null)
    {
        $atts = shortcode_atts(q__::$q_defaults, $atts, 'q_');
        $objs = q__::query($atts);

        ob_start();

        if (!empty($objs)) {
            
            if (empty($content)) { $content = q__::layout; }
            
            echo q__::parseContent($content, $objs);

        } else {
            echo "<p><em>Sorry, nothing was found.</em></p>";
        }
        
        $response = ob_get_clean();
            
        return $response;
    }
    
    /* 
     * Returns array of objects
     */
    public static function query($args = array()) 
    {
        q__::prepQuery($args);

        $newq = new WP_Query($args);
        $response = array();
        
        if ($newq->have_posts()) {
            while($newq->have_posts()) {
                $newq->the_post();
                global $post;
                $response[] = $post;
            }
        }
        
        wp_reset_postdata();
        
        return $response;
    }
    private static function prepQuery(&$args)
    {
        $args = wp_parse_args($args, q__::$q_defaults);

        /* figure out exclusions */
        q__::sString($args['author']);
        q__::sArray($args['author_id'], 'intval');
        q__::move($args, 'author_id', 'author__in');

        q__::sString($args['category']);
        q__::sArray($args['category_id'], 'intval');
        q__::move($args, 'category_id', 'category__in');
        
        q__::sString($args['tag']);
        q__::sArray($args['tag_id'], 'intval');
        q__::move($args, 'tag_id', 'tag__in');
        
        q__::sString($args, 'search', 's');

        q__::sArray($args['id'], 'intval');
        q__::move($args, 'id', 'post__in');
        
        q__::sString($args, 'name');
        
        q__::sFirst($args['parent'], 'intval');
        q__::move($args, 'parent', 'post_parent');
        
        q__::sString($args, 'type', 'post_type');
        
        q__::sInArray($args, 'status', array('publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash', 'any'));
        q__::move($args, 'status', 'post_status');
        
        q__::sFirst($args['count'], 'intval');
        q__::move($args, 'count', 'posts_per_page');
        
        q__::sBool($args['ignore_sticky']);
        q__::move($args, 'ignore_sticky', 'ignore_sticky_posts');
        
        q__::sInArray($args, 'order', array('ASC', 'DESC'));
        q__::sInArray($args, 'orderby', array('none', 'ID', 'author', 'title', 'name', 'type', 'date', 'modified', 'parent', 'rand', 'comment_count', 'menu_order'));
                        
        $args = array_filter($args);
    }
    public static function move(&$args, $index, $new_index)
    {
        $val = $args[$index];
        $args[$new_index] = $val;
        unset($args[$index]);
    }
    public static function sFirst(&$arr, $sanitizer = 'sanitize_text_field', $delim = ',')
    {
        q__::sArray($arr, $sanitizer, $delim);
        $arr = array_shift($arr);
    }
    public static function sArray(&$arr, $sanitizer = 'sanitize_text_field', $delim = ',')
    {
        if (!is_array($arr))
            $arr = explode($delim, $arr);
        
        if (!empty($arr)) {
            $r = array();
            foreach($arr as $i) {
                $i = $sanitizer($i);
                if (!empty($i))
                    $r[] = $i;
            }
            $arr = $r;
        }
    }
    public static function sString(&$val, $oldIndex = '', $newIndex = '')
    {
        if (is_array($val)) {
            $str = sanitize_text_field($val[$oldIndex]);
            if (empty($newIndex)) {
                $val[$oldIndex] = $str;
            } else {
                $val[$newIndex] = $str;
                unset($val[$oldIndex]);
            }
        } elseif (is_string($val)) {
            $val = sanitize_text_field($val);
        }
    }
    public static function sBool(&$val)
    {
        $val = !empty($val);
    }
    public static function sInArray(&$args, $index, $approved)
    {
        q__::sString($args[$index]);
        if (!in_array($args[$index], $approved))
            $args[$index] = q__::$q_defaults[$index];
    }
    public static function firstImage($html)
    {
        $images = array();
        preg_match("/img.*src=['\"](.*)(-\d+x\d+)?\.(jpg|png|gif|jpeg)['\"]/iU", $html, $images);
        
        if (!empty($images))
            return esc_url("{$images[1]}.{$images[3]}");
        
        return false;
    }
    public static function mediaID($url)
    {        
        global $wpdb;
        
        $url = apply_filters('q__media_id_url', $url);
        $url = esc_url($url);
        $url = parse_url($url, PHP_URL_PATH);
        $url = basename($url);
        
        $query = "SELECT ID FROM {$wpdb->posts} WHERE guid LIKE '%{$url}'";
        
        $id = $wpdb->get_results($query, ARRAY_A);
        
        if (empty($id))
            return false;
        
        while(is_array($id))
            $id = array_shift($id);
        
        return intval($id);
    }
    public static function parseContent($content, $objs)
    {
        if (empty($objs))
            return '<p>Sorry, nothing was found.</p>';
        
        if (empty($content))
            $content = q__::layout;
        
        ob_start();
        
        $content = html_entity_decode($content);
        $content = str_replace(array('’', '′'), '"', $content);
        
        $sc = new q__sc();

        if (is_array($objs)) {
            global $post;
            foreach($objs as $obj) {
                $post = $obj;
                setup_postdata($obj);
                echo do_shortcode($content);
            }
        } else {
            global $post;
            $post = $objs;
            setup_postdata($objs);
            echo do_shortcode($content);
        }
        
        wp_reset_postdata();
        
        $sc->destruct();

        return ob_get_clean();
    }
}

global $qq;
$qq = new q__();

function do_qquery($args = array(), $content = null) {
    $query = q__::query($args);
    
    if (!empty($content))
        return q__::parseContent($content, $query);

    return $query;
}

include 'q__sc.php';
include 'widgets/widgets.php';
