<?php

class q__sc
{
    private $shortcodes = array(
        'q_title', 'q_url', 'q_link', 'q_content', 'q_excerpt', 'q_date',
        'q_author', 'q_term', 'q_status', 'q_type', 'q_id', 'q_image'
    );
    public function __construct()
    {
        $shortcodes = apply_filters('q__default_shortcodes', $this->shortcodes);
        
        foreach($shortcodes as $sc)
            add_shortcode($sc, array($this, $sc));
        
        do_action('q__add_shortcodes');
    }
    public function destruct() 
    {
        $shortcodes = apply_filters('q__default_shortcodes', $this->shortcodes);
        
        foreach($shortcodes as $sc)
            remove_shortcode($sc);
        
        do_action('q__remove_shortcodes');
    }

    public function q_title()
    {
        return get_the_title();
    }
    public function q_link()
    {
        return "<a href='" . $this->q_url() . "'>" . $this->q_title() . "</a>";
    }
    public function q_url()
    {
        return get_the_permalink();
    }
    public function q_content()
    {
        return get_the_content();
    }
    public function q_excerpt($atts)
    {
        $atts = shortcode_atts(array(
            'words' => '', // words
            'length' => '', // characters
            'after' => '...',
            'link' => 'true'
        ), $atts, 'q_excerpt');
        
        $after = '';
        if (!empty($atts['after'])) {
            $after = $atts['after'];
            if ($atts['link'] === 'true') {
                $link = get_the_permalink();
                $after = "<a href='{$link}'>{$after}</a>";
            }
        }
        
        if (!empty($atts['words'])) {
            $content = get_the_content();
            $content = strip_tags($content);
            $content = explode(' ', $content);
            $words = intval($atts['words']) ?: 55;
            $content = array_slice($content, 0, $words);
            return implode(' ', $content) . $after;
        }
        
        if (!empty($atts['length'])) {
            $content = get_the_content();
            $content = strip_tags($content);
            $length = intval($atts['length']) ?: 225;
            $content = substr($content, 0, $length);
            return $content . $after;
        }
        
        return get_the_excerpt();
    }
    public function q_date($atts)
    {
        $atts = shortcode_atts(array( 'format' => null ), $atts, 'q_date');
        return get_the_date($atts['format']);
    }
    public function q_author()
    {
        return get_the_author();
    }
    public function q_term($atts)
    {
        $atts = shortcode_atts(array( 'term' => 'category' ), $atts, 'q_term');
        $t = get_the_term_list(get_the_id(), sanitize_text_field($atts['term']), '', ', ', '');
        return $t ?: '';
    }
    public function q_status()
    {
        return get_post_status();
    }
    public function q_type()
    {
        return get_post_type();
    }
    public function q_id()
    {
        return get_the_ID();
    }
    
    
    private function imageFromFeature()
    {
        if (has_post_thumbnail()) {
            return get_post_thumbnail_id();
        } else {
            $iurl = q__::firstImage(get_the_content());
            $nid = q__::mediaID($iurl);

            if ($nid)
                return $nid;
            elseif (!empty($iurl))
                return $iurl;
        }
        return false;
    }
    private function imageFromFirst()
    {
        $iurl = q__::firstImage(get_the_content());
        $nid = q__::mediaID($iurl);
        if ($nid)
            return $nid;
        elseif (!empty($iurl))
            return $iurl;
        elseif (has_post_thumbnail())
            return get_post_thumbnail_id();
        return false;
    }
    
    public function q_image($atts)
    {
        $atts = shortcode_atts(array(
            'placeholder' => '',
            'from' => '',
            'class' => '',
            'size' => '',
        ), $atts, 'q_image');
        
        foreach($atts as $k => $v)
            $atts[$k] = trim($v, '”');
        
        q__::sArray($atts['class'], 'sanitize_html_class', ' ');
        $atts['class'] = implode(' ', $atts['class']);
        
        q__::sString($atts['size']);
        if ($atts['size'] === 'thumb')
            $atts['size'] = 'thumbnail';
        
        $allowed_sizes = get_intermediate_image_sizes();
        $allowed_sizes[] = 'full';
        
        if (!in_array($atts['size'], $allowed_sizes)) {
            $tsize = explode(',', $atts['size']);
            $tsize = array_map('intval', $tsize);
            
            if (count($tsize) === 1)
                $atts['size'] = array($tsize[0], $tsize[0]);
            elseif (count($tsize) === 2)
                $atts['size'] = $tsize;
            else
                $atts['size'] = 'full';
        }
        
        switch($atts['from']) {
            case 'feature':
                $image = $this->imageFromFeature();
                break;
            case 'first':
                $image = $this->imageFromFirst();
                break;
            default:
                if (get_post_type() === 'attachment')
                    $image = get_the_ID();
                else
                    $image = $this->imageFromFeature();
                break;
        }
        
        if (empty($image)) {
            if (intval($atts['placeholder']))
                $image = intval($atts['placeholder']);
            else
                $image = $atts['placeholder'];
        }
        
        if (!filter_var($image, FILTER_VALIDATE_URL)) {
            if (intval($image)) {
                $src = wp_get_attachment_image_src($image, $atts['size']);
                $image = $src[0];
            } else {
                $p = parse_url($image, PHP_URL_PATH);
                $h = parse_url($image, PHP_URL_HOST);
                if (empty($h) && !empty($p)) {
                    $image = site_url() . $image;
                }
            }
        }
        
        if (empty($image)) {
            $w = get_option("{$atts['size']}_size_w", 512);
            $h = get_option("{$atts['size']}_size_h", 512);
            $image = "https://placehold.it/{$w}x{$h}";
        }

        $response = '<img src="' . esc_url($image) . '" class="' . $atts['class'] . '" />';

        return $response;
    }
}