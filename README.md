# README #

### What is this repository for? ###

* Embedding, formatting, and displaying queries in WordPress content.

### How do I get set up? ###

* Install in /wp-content/plugins/

### Contribution guidelines ###

* Don't be evil.

### Who do I talk to? ###

* [click to email me](mailto:tdhasenpflug@gmail.com)